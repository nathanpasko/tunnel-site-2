import React, { useEffect, useState } from "react";
import { Link } from "gatsby";

export default function Layout({ children, location }) {
  const [navOpen, setNavOpen] = useState(false);
  const [scrolled, setScrolled] = useState(false);
  const [scrollListener, setScrollListener] = useState();
  const [elem, setElem] = useState();

  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this;
      var args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  const toggleNav = () => {
    const val = !navOpen;
    setNavOpen(val);
  };

  const checkScroll = debounce(function () {
    console.log("s");
    if (elem) {
      let rect = elem.getBoundingClientRect();
      // set scrolled to value
      var threshold = -(rect.height * 0.88);
      let val = rect.y < threshold ? true : false;
      setScrolled(val);
    }
  }, 100);

  useEffect(() => {
    var e = document.querySelector("#masthead");
    setElem(e);
  }, [elem]);

  useEffect(() => {
    var listener = document.addEventListener("scroll", checkScroll);
    setScrollListener(listener);
    document.addEventListener("resize", checkScroll);
    document.addEventListener("orientationchange", checkScroll);
  }, [scrollListener]);

  return (
    <>
      <header>
        <a href="/">
          <picture id="masthead" className={location ? "" : "logo"}>
            {location ? (
              <svg
                id="Layer_1"
                data-name="Layer 1"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 396.6 396.6"
              >
                <defs>
                  <style>
                    {`.cls-1{fill:#ff0;}`}
                    {`.cls-2{fill:blue;}`}
                  </style>
                </defs>
                <g id="tunnel">
                  <path d="M94.2,98.8H69.3v65.4h-36V98.8H6V63.4H94.2Z" />
                  <path className="cls-1" d="M69.3,164.2V267.6h-36V164.2" />
                  <path d="M241.3,267.6h77v65.3h72.3V297.5H354.3V232.1H304.4V210.6H276.7v-8.4h41.6v-38h-77V60.8h-36V112l-52-51.2V164.2h52v51.2l-52-51.2H116.7v65.6H105.9V164.2H69.3V217c0,17.2,0,49.6,42.1,49.6s41.9-32.4,41.9-49.6h0v50.6" />
                </g>
                <g id="engine">
                  <path
                    className="cls-2"
                    d="M126.7,310.7v.2c0,5.3,3.8,8.6,8.1,8.6a8.6,8.6,0,0,0,5.9-2.3l.6.8a9.5,9.5,0,0,1-6.5,2.5c-4.9,0-9-3.6-9-9.5s3.9-9.3,8.4-9.3,7.4,3.3,7.4,7.7v1.3Zm.1-1h13.8c0-5.2-3.1-7.1-6.4-7.1S127.3,304.8,126.8,309.7Z"
                  />
                  <path
                    className="cls-2"
                    d="M154.7,302.1h.9v3.5a8.9,8.9,0,0,1-.1,1.3h0a8,8,0,0,1,7.8-5.2c4.5,0,6.2,2.4,6.2,7.1v11.3h-1V309c0-3.5-.8-6.4-5.2-6.4a7.6,7.6,0,0,0-7.4,5.8,9.3,9.3,0,0,0-.3,2.4v9.3h-.9Z"
                  />
                  <path
                    className="cls-2"
                    d="M184.6,325.2a10.1,10.1,0,0,0,5.1,1.3c3.7,0,7-1.9,7-6.5v-2.7a6.1,6.1,0,0,1,.2-1.5h-.1a6.7,6.7,0,0,1-6.4,4.3c-4.7,0-8-3.7-8-9.4s3-9,7.8-9c3,0,5.8,1.4,6.6,4.5h.1a3.1,3.1,0,0,1-.2-1v-3.1h1V320c0,5.4-3.8,7.5-8,7.5A12.7,12.7,0,0,1,184,326Zm12.2-14.4c0-6.2-3-8.2-6.6-8.2s-6.8,3-6.8,8.1,2.8,8.4,7,8.4S196.8,317,196.8,310.8Z"
                  />
                  <path
                    className="cls-2"
                    d="M212,294.8h1v3.4h-1Zm0,7.3h1v18h-1Z"
                  />
                  <path
                    className="cls-2"
                    d="M227.3,302.1h1v3.5a4.6,4.6,0,0,1-.2,1.3h.1a8,8,0,0,1,7.8-5.2c4.5,0,6.1,2.4,6.1,7.1v11.3h-.9V309c0-3.5-.9-6.4-5.2-6.4a7.5,7.5,0,0,0-7.4,5.8,9.3,9.3,0,0,0-.3,2.4v9.3h-1Z"
                  />
                  <path
                    className="cls-2"
                    d="M255.9,310.7v.2c0,5.3,3.8,8.6,8.1,8.6a8.6,8.6,0,0,0,5.9-2.3l.6.8a9.5,9.5,0,0,1-6.5,2.5c-4.9,0-9-3.6-9-9.5s3.9-9.3,8.4-9.3,7.4,3.3,7.4,7.7v1.3Zm.1-1h13.8c0-5.2-3.1-7.1-6.4-7.1S256.5,304.8,256,309.7Z"
                  />
                </g>
              </svg>
            ) : (
              <svg
                id="Layer_1"
                data-name="Layer 1"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 396.6 396.6"
              >
                <defs>
                  <style>{`.cls-1{fill:#ff0;}`}</style>
                </defs>
                <g id="tunnel">
                  <path d="M94.2,98.8H69.3v65.4h-36V98.8H6V63.4H94.2Z" />
                  <path className="cls-1" d="M69.3,164.2V267.6h-36V164.2" />
                  <path d="M241.3,267.6h77v65.3h72.3V297.5H354.3V232.1H304.4V210.6H276.7v-8.4h41.6v-38h-77V60.8h-36V112l-52-51.2V164.2h52v51.2l-52-51.2H116.7v65.6H105.9V164.2H69.3V217c0,17.2,0,49.6,42.1,49.6s41.9-32.4,41.9-49.6h0v50.6" />
                </g>
              </svg>
            )}
          </picture>
        </a>
      </header>
      <picture className={scrolled ? "logo-small active" : "logo-small"}>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 259.2 259.2">
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <path d="M129.6,0A129.6,129.6,0,1,0,259.2,129.6,129.6,129.6,0,0,0,129.6,0Zm61.8,199.6V166h-85V140c0,8.8.1,25.5-21.5,25.5S63.2,148.8,63.2,140V112.8H44.7V79.1H30.6V60.9H76V79.1H63.2v33.7H82v33.8h5.6V112.8h18.8V59.6l26.8,26.3V59.6h18.5v53.2h39.7v19.6H170v4.3h14.2v11h25.7v33.7h18.7v18.2Z" />
              <polygon points="133.2 112.8 106.4 112.8 133.2 139.2 133.2 112.8" />
            </g>
          </g>
        </svg>
      </picture>
      <nav className={navOpen ? "active" : ""}>
        <ul onClick={() => toggleNav()} onKeyDown={() => toggleNav()}>
          <li>
            <Link to={`/`}>About Tunnel</Link>
          </li>
          <li>
            <Link to={`/manual/`}>User's Manual</Link>
          </li>
          <li>
            <Link to={`/download`}>Download</Link>
          </li>
          <li>
            <button onClick={() => setNavOpen(false)}>Close</button>
          </li>
        </ul>
      </nav>
      {children}
      <footer>
        <p className="centered">
          Site &copy; Nathan Pasko. Tunnel is open source under the MIT License.
        </p>
      </footer>
    </>
  );
}
