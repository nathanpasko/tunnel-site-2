import React from "react";
import Layout from "../components/layout";

export default function Download() {
  return (
    <Layout>
      <section>
        <h4>Download Tunnel</h4>
        <p className="large">
          Here's how you can download the latest official release of Tunnel.
        </p>
        <p>
          Grab one of these downloads if you want to start making your own
          Tunnel cartridges. Every cartridge includes the core Tunnel files, so
          if you just want to play cartridges, all you need is a web browser.
        </p>
      </section>
      <section>
        <h4>Get Tunnel via GitLab</h4>
        <p>
          Download the Tunnel engine core from the official git repository on
          GitLab.
        </p>
        <p>
          <a href="https://gitlab.com/nathanpasko/tunnel-engine">Download via GitLab</a>
        </p>
      </section>
      <section>
        <h4>Get Tunnel via itch.io</h4>
        <p>
          The Tunnel engine core is also mirrored on itch.io by A.V. Knights.
        </p>
        <p>
          <a href="https://avknights.itch.io/tunnel">Download via itch.io</a>
        </p>
      </section>
    </Layout>
  );
}
