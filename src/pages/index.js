import React, { useEffect } from "react"
import Layout from "../components/layout"

export default function Home({location}) {
  return (
    <Layout location={location}>
      <section>
        <h4>What is Tunnel?</h4>
        <p className="large">A temporally-unopinionated audio/video engine for the web.</p>
        <p>Create, trade, remix, and share Tunnel cartridges. Each cartridge is composed of layers of sound, images, and color. The Tunnel engine is a new format designed to let the audience control the timing and structure of interactive artworks. Everything is open source and responsive.</p>
      </section>
      <section>
        <h4>Why Tunnel?</h4>
        <p>Tunnel was designed to encapsulate a particular personality. It doesn't do a lot of things. Tunnel aims for specificity rather than wide capability. One reason for this is to carve out an identity for Tunnel as a format. Another reason is to keep Tunnel compact, polished, and maintainable.</p>
      </section>
      <section>
        <h4>Who made Tunnel?</h4>
        <p>This project was created by Nathan Pasko—a designer, developer, writer, and musician. You can <a href="http://nathanpasko.com">click here to visit Nathan on the web</a> and learn more.</p>
      </section>
    </Layout>
  )
}
