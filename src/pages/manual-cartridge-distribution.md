###### User's Manual
## Cartridge Distribution

How you distribute your cartridges is up to you, but remember Tunnel is open source under the MIT license, and also that moving a cartridge to a web server will probably provide the most consistent experience. You might've noticed that this website is hosted by [Netlify](https://netlify.com).
___
\
Previous: **[Creator Tips](/manual-creator-tips/)**\
Back: **[Table of Contents](/manual/)**
___