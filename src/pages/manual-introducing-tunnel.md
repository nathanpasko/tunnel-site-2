###### User's Manual
## Introducing Tunnel

_Tunnel is a new format._

It’s related to audio formats like cassettes or vinyl records, and video formats like slideshows or visualizers. Similar to the way we enjoy aspects of older audio formats such as hiss or warmth, Tunnel’s idiosyncrasies define and specify it. The goal isn’t to replace extant formats, but rather to introduce a complimentary alternative. Tunnel has its own shape, just like a DVD or an 8 track does.

Tunnel cartridges are the bundles of sound and imagery that we create, playback, share, and remix to express ourselves.

### Acknowledgements

Tunnel was inspired by the likes of [Pico-8](https://www.lexaloffle.com/pico-8.php) and [Pet Minotaur](https://github.com/jakevsrobots/minotaur), by mixtapes and the banality of music streaming services, and by past work with micro-labels [Apartment 421](https://apartment421tapes.bandcamp.com/music) and [HVR](https://hiddenvisionrecords.bandcamp.com).
___
\
Next: **[Cartridges](/manual-cartridges/)**\
Previous: **[Table of Contents](/manual/)**
___