###### User's Manual
## Making a Tunnel Cartridge

If you're an artist or a musician, you're probably wondering how to put together a new Tunnel cartridge. If you're a programmer, you'll find the process quite straightforward.

### Quick Start

**For people comfortable with HTML and JavaScript.** More in-depth instructions below.

1. Create a directory called **cart**.

2. Copy Tunnel into **cart**.

3. Open **index.html** in browser to see empty message.

4. Create **cartridge.js** in **cart**.

5. Write `info` object—see **Info Settings** in [Cartridge Data](/manual-cartridge-data).

6. Write `layers` array—see **Layer Settings** in [Cartridge Data](/manual-cartridge-data).

7. Embed **cartridge.js** into **index.html**.

8. Refresh **index.html** in browser to see loaded cartridge.

9. Add image and sound assets to directory and metadata to layers.

### Getting Ready

Tunnel is a web-based format, and what we call “cartridges” are really just a metaphor; this terminology lends a flavor in conversation with Tunnel’s guiding principles.

_In literal terms, a cartridge is a collection of images, sounds, and metadata in a folder or directory with the Tunnel core._

Before you sit down to assemble a cartridge, try to plan out each layer and prepare the required source files. 

### Using JavaScript

To write the data file for a cartidge in JavaScript, create a .js file and declare two objects inside: `info` and `layers`.

Then, simply fill those objects with your desired settings.

```javascript
const info = {
  title: 'Cartridge Title',
  labels: true,
  nav: [
    { text: 'Link 1', href: 'https://tunnelengine.netlify.app' },
    { text: 'Link 2', href: 'http://nathanpasko.com' }
  ]
}
const layers = [
  { id: 101, images: ['', '', '', ''] },
  { id: 202, images: ['', '', '', ''] },
  { id: 303, images: ['', '', '', ''] }
]
```

Your .js file needs to have an object called `info` and an array called `layers`. Use keys and values of `info` to apply **Info Settings** listed in [Cartridge Data](/manual-cartridge-data/). 

If you wish to fill the cartridge nav with hyperlinks, fill an array at `info.nav` with nav link objects and their settings; see the **Nav Link Settings** listed above. 

To add layers to your cartridge, compose layer objects by declaring their attributes with the Layer Settings above. Your layers must be set in order inside the `layers` array. Every cartridge needs at least 3 layers. If you don't include 3 layers in your cartridge, Tunnel will generate them for you.

The Tunnel core is set to find your settings once you “load your cartridge”, that is, embed your file in the HTML. Look at the end of the <body> tag for a spot to embed your cartridge data file.

```html
<!--
  ◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼◼︎◼︎
  LOAD CATRIDGE HERE 
-->


<!-- 
  CARTRIDGE ABOVE 
  ◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼◼︎◼︎
-->
```

Use a `<script>` tag with the `id` set to `cartridge`.

```html
<script id=“cartridge” src=“/path/to/file.js”></script>
```

The cartridge is all done! Open **index.html** in a browser to test.

### Using Cartridge Maker

Like the rest of Tunnel, the Cartridge Maker is fully responsive, though this tool in particular is geared slightly more toward desktop machines. It will export a JSON file that you can paste into the Tunnel core instead of writing a JavaScript data file by hand.

You will need to start your cartridge by copying Tunnel into a folder or directory with your audio and image assets.

Use the Cartridge Maker to enter settings and content for your cartridge. 

When you’re ready, click the Finish button to download a JSON file. Copy the entire contents of that file to the clipboard. It’s a representation of the settings you declared in the Cartridge Maker.

Now open the HTML Tunnel core, **index.html**. Near the bottom of the code, you’ll see a spot before the closing `</body>` tag where you can load your cartridge. 

```html
    <!--
      ◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼◼︎◼︎
      LOAD CATRIDGE HERE 
    -->
    

    <!-- 
      CARTRIDGE ABOVE 
      ◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼︎◼◼︎◼︎
    -->
```

Use a `<script>` tag with `id="cartridge"` and `type="application/json"`. Inside, add your data via clipboard paste.

```html
<script id=“cartridge” type=“application/json”>
  <!-- paste your data here -->
</script>
```

Your cartridge is all loaded! Open **index.html** in a browser to test.
___
\
Next: **[Creator Tips](/manual-creator-tips/)**\
Previous: **[Cartridges](/manual-cartridges/)**\
Back: **[Table of Contents](/manual/)**
___