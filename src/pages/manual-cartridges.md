###### User's Manual
## Cartridges

Tunnel is a delivery system for the content distributed in cartridges. There's not really any point to running Tunnel without a cartridge. The Tunnel engine is built to frame cartridges with a certain personality, but the cartridges themselves are the objects of desire.

### Playing a Tunnel Cartridge

_Tunnel is designed for the web, so playing a Tunnel cartridge is effortless._

Loading the cartridge at a web server is as simple as typing the URL into a browser and pressing Go.

Tunnel uses the WebAudio API, which will **require iOS users to turn their Ringers on** to hear cartridges.

### Layers

Cartridges are organized in layers. Each layer is a convergence of a looping sound clip and 4 semitransparent image slides with optional background colors. Layers are stacked in fixed order and the cartridge entry point is at layer 1. Layer 0 is the control layer, partially reserved by the engine. 

### Controls

_The top half of the screen is a button moving the user outward._

_The bottom half of the screen is a button moving the user inward._

These are the controls for traversing a Tunnel cartridge.

### Labels

If a cartridge has standard labels on it, there will be a display showing the cartridge title and a number of hyperlinks.
___
\
Next: **[Making a Tunnel Cartridge](/manual-making-a-tunnel-cartridge/)**\
Previous: **[Introducing Tunnel](/manual-introducing-tunnel/)**\
Back: **[Table of Contents](/manual/)**
___