###### User's Manual
## Creator Tips

_Here are a few tips on how to create the best cartridges possible._

- Use just a couple links in cart nav
- Try using images with transparent areas
- Smaller files download faster
- Check both light & dark display modes on your device

These tips arise from the idiosyncrasies of the Tunnel format, and though today they are "best practices", may provide a roadmap for future releases to specify the format even further.
___
\
Next: **[Cartridge Distribution](/manual-cartridge-distribution/)**\
Previous: **[Making a Tunnel Cartridge](/manual-making-a-tunnel-cartridge/)**\
Back: **[Table of Contents](/manual/)**
___