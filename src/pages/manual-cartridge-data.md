###### User's Manual
## Cartridge Data

Every Tunnel cartridge is a combination of the Tunnel core, audio and image assets, and a data file with information about the cartridge and each layer within. This data file is the link between the core Tunnel engine and the raw assets used by the cartridge.

### What's in the data file?

A cartridge’s data file includes info data and layer data. **Info** is a group of settings that govern the cartridge’s title, and appearance. **Layers** is an ordered list declaring every detail of the cartridge’s layers. Some of the settings available here are required, others are optional.

#### Info Settings

| Key | Description |
| :-- | :---------- |
| `title` | **REQUIRED.** The public-facing title of the cartridge, i.e. the album title. |
| `labels` | If labels is set to true, the cartridge will display standardized labels in a compact header. |
| `nav` | A list of hyperlinks that display as part of the standardized cartridge labels. |

#### Nav Link Settings

| Key | Description |
|:--- | :---------- |
| `text` | **REQUIRED.** The textual label for the link. |
| `href` | **REQUIRED.** The URL that the link will navigate to. |

#### Layer Settings

| Key | Description |
|:--- |:----------- |
| `id` | **REQUIRED.** An internal number identifier used by Tunnel. |
| `background` | Add a color background instead of default transparency. |
| `audio` | Add a sound clip that will loop while this layer is focused. |
| `images` | **REQUIRED.** An array of four URLs for images forming this layer (or empty strings for no image).
___
\
Related: **[Making a Tunnel Cartridge](/manual-making-a-tunnel-cartridge/)**\
Back: **[Table of Contents](/manual/)**
___
