# User's Manual

## Table of Contents

## Introducing Tunnel

Tunnel is a new format.

It’s related to audio formats like cassettes or vinyl records, and video formats like slideshows or visualizers. Similar to the way we enjoy aspects of older audio formats such as hiss or warmth, Tunnel’s idiosyncrasies define and specify it. The goal isn’t to replace extant formats, but rather to introduce a complimentary alternative. Tunnel has its own shape, just like a DVD or an 8 track does.

Tunnel cartridges are the bundles of sound and imagery that we create, playback, share, and remix to express ourselves.

## Cartridges

Tunnel is a delivery system for the content distributed in cartridges. There's not really any point to running Tunnel without a cartridge. The Tunnel engine is built to frame cartridges with a certain personality, but the cartridges themselves are the objects of desire.

### Playing a Tunnel Cartridge

Tunnel is designed for the web, so playing a Tunnel cartridge is effortless: loading the cartridge at a web server is as simple as typing the URL into a browser and pressing Go.

### Layers

Cartridges are organized in layers. Each layer is a convergence of a looping sound clip and 4 semitransparent image slides with optional background colors. Layers are stacked in fixed order and the cartridge entry point is at layer 1. Layer 0 is the control layer, partially reserved by the engine. 

### Controls

The top half of the screen is a button moving the user outward. 

The bottom half of the screen is a button moving the user inward. 

These are the controls for traversing a Tunnel cartridge.

### Labels

If a cartridge has standard labels on it, there will be a display showing the cartridge title and a number of hyperlinks.

## Making a Tunnel Cartridge

If you're an artist or a musician, you're probably wondering how to put together a new Tunnel cartridge. If you're a programmer, you'll find the process quite straightforward.

### Quick Start

**For people comfortable with HTML and JavaScript.** More in-depth instructions below.

1. Create a directory called **cart**.

2. Copy Tunnel into **cart**.

3. Open **index.html** in browser to see empty message.

4. Create **cartridge.js** in **cart**.

5. Write const info.

6. Write layers.

7. Embed **cartridge.js** into **index.html**.

8. Refresh **index.html** in browser to see loaded cartridge.

9. Add image and sound assets to directory and metadata to layers.

### Getting Ready

Tunnel is a web-based format, and what we call “cartridges” are really just a metaphor; this terminology lends a flavor in conversation with Tunnel’s guiding principles.

In literal terms, a cartridge is a collection of images, sounds, and metadata in a folder or directory with the Tunnel core. Before you sit down to assemble a cartridge, try to plan out each layer and prepare the required source files. 

### What data are we working with?

A cartridge’s data file includes info data and layer data. **Info** is a group of settings that govern the cartridge’s title, and appearance. **Layers** is an ordered list declaring every detail of the cartridge’s layers. Some of the settings available here are required, others are optional.

#### Info Settings

| Key | Description |
| :-- | :---------- |
| `title` | **REQUIRED.** The public-facing title of the cartridge, i.e. the album title. |
| `labels` | If labels is set to true, the cartridge will display standardized labels in a compact header. |
| `nav` | A list of hyperlinks that display as part of the standardized cartridge labels. |

#### Nav Link Settings

| Key | Description |
|:--- | :---------- |
| `text` | **REQUIRED.** The textual label for the link. |
| `href` | **REQUIRED.** The URL that the link will navigate to. |

#### Layer Settings

| Key | Description |
|:--- |:----------- |
| `id` | **REQUIRED.** An internal number identifier used by Tunnel. |
| `background` | Add a color background instead of default transparency. |
| `audio` | Add a sound clip that will loop while this layer is focused. |
| `images` | **REQUIRED.** A list of four URLs for images forming this layer (or empty slots for no image).

### JavaScript

To write the data file for a cartidge in JavaScript, create a .js file and declare two objects inside: info and layers.

Then, simply fill those objects with your desired settings.

```javascript
const info = {
  title: 'Cartridge Title',
  labels: true,
  nav: [
    { text: 'Link 1', href: 'https://tunnelengine.netlify.app' },
    { text: 'Link 2', href: 'http://nathanpasko.com' }
  ]
}
const layers = [
  { id: 101, images: ['', '', '', ''] },
  { id: 202, images: ['', '', '', ''] },
  { id: 303, images: ['', '', '', ''] }
]
```

Your .js file needs to have an object called `info` and an array called `layers`. Use keys and values of `info` to apply **Info Settings** listed above. 

If you wish to fill the cartridge nav with hyperlinks, fill an array at `info.nav` with nav link objects and their settings; see the **Nav Link Settings** listed above. 

To add layers to your cartridge, compose layer objects by declaring their attributes with the Layer Settings above. Your layers must be set in order inside the `layers` array. Every cartridge needs at least 3 layers. If you don't include 3 layers in your cartridge, Tunnel will generate them for you.

The Tunnel core is set to find your settings once you “load your cartridge”, that is, embed your file in the HTML. Look at the end of the <body> tag for a spot to embed your cartridge data file.

```html
<script id=“cartridge” src=“/path/to/file.js”></script>
```

The cartridge is all done! Open `index.html` in a browser to test.


### Cartridge Maker

Like the rest of Tunnel, the Cartridge Maker is fully responsive, though this tool in particular is geared slightly more toward desktop machines. It will export a JSON file that you can paste into the Tunnel core instead of writing a JavaScript data file by hand.

You will need to start your cartridge by copying Tunnel into a folder or directory with your audio and image assets.

Use the Cartridge Maker to enter settings and content for your cartridge. 

When you’re ready, click the Finish button to download a JSON file. Copy the entire contents of that file to the clipboard. It’s a representation of the settings you declared in the Cartridge Maker.

Now open the HTML Tunnel core, `index.html`. Near the bottom of the code, you’ll see a spot before the closing `</body>` tag where you can load your cartridge. 

```html
cartridge here
```

Use a `<script>` tag to add your data via clipboard paste.

```html
<script id=“cartridge” type=“application/json”>
  <!-- paste your data here -->
</script>
```

Your cartridge is all loaded! Open index.html in a browser to test.

### Node

A Node app to build cartridges is currently in development. 😮

## Creator Tips

Here are a few tips on how to create the best cartridges possible.

- Use just a couple links in cart nav
- Try using images with transparent areas
- Smaller files download faster
- Check both light & dark display modes on your device

These tips arise from the idiosyncrasies of the Tunnel format, and though today they are "best practices", may provide a roadmap for future releases to specify the format even further.

## Cartridge Distribution

How you distribute your cartridges is up to you, but remember Tunnel is open source under the MIT license, and also that moving a cartridge to a web server will probably provide the most consistent experience.